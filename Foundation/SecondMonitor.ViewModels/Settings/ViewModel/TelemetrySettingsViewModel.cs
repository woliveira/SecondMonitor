﻿namespace SecondMonitor.ViewModels.Settings.ViewModel
{
    using Model;

    public class TelemetrySettingsViewModel : AbstractViewModel<TelemetrySettings>
    {
        private bool _isTelemetryLoggingEnabled;
        private int _loggingInterval;
        private int _maxSessionsKept;
        private bool _logInvalidLaps;
        private bool _logBestForEachDriver;
        private bool _logOpponentsBest;
        private bool _logOnlyPlayerClass;

        public bool IsTelemetryLoggingEnabled
        {
            get => _isTelemetryLoggingEnabled;
            set => SetProperty(ref _isTelemetryLoggingEnabled, value);
        }
        public int LoggingInterval
        {
            get => _loggingInterval;
            set => SetProperty(ref _loggingInterval, value);
        }

        public int MaxSessionsKept
        {
            get => _maxSessionsKept;
            set => SetProperty(ref _maxSessionsKept, value);
        }

        public bool LogInvalidLaps
        {
            get => _logInvalidLaps;
            set => SetProperty(ref _logInvalidLaps, value);
        }

        public bool LogOpponentsBest
        {
            get => _logOpponentsBest;
            set => SetProperty(ref _logOpponentsBest, value);
        }

        public bool LogBestForEachDriver
        {
            get => _logBestForEachDriver;
            set => SetProperty(ref _logBestForEachDriver, value);
        }

        public bool LogOnlyPlayerClass
        {
            get => _logOnlyPlayerClass;
            set => SetProperty(ref _logOnlyPlayerClass, value);
        }


        protected override void ApplyModel(TelemetrySettings model)
        {
            IsTelemetryLoggingEnabled = model.IsTelemetryLoggingEnabled;
            LoggingInterval = model.LoggingInterval;
            MaxSessionsKept = model.MaxSessionsKept;
            LogInvalidLaps = model.LogInvalidLaps;
            LogOpponentsBest = model.LogOpponentsBest;
            LogBestForEachDriver = model.LogBestForEachDriver;
            LogOnlyPlayerClass = model.LogOnlyPlayerClass;
        }

        public override TelemetrySettings SaveToNewModel()
        {
            return new TelemetrySettings()
            {
                IsTelemetryLoggingEnabled = IsTelemetryLoggingEnabled,
                LoggingInterval = LoggingInterval,
                MaxSessionsKept = MaxSessionsKept,
                LogInvalidLaps = LogInvalidLaps,
                LogOpponentsBest = LogOpponentsBest,
                LogBestForEachDriver = LogBestForEachDriver,
                LogOnlyPlayerClass = LogOnlyPlayerClass,
            };
        }
    }
}