﻿namespace SecondMonitor.Contracts
{
    using System.Collections.Generic;
    using System.Linq;

    public abstract class AbstractHumanReadableMap<T>
    {
        protected AbstractHumanReadableMap()
        {
            TranslationList = new List<(T value, string humanReadableValue)>();
        }

        protected List<(T value, string humanReadableValue)> TranslationList { get; set; }

        public string ToHumanReadable(T value)
        {
            return TranslationList.First(x => x.value.Equals(value)).humanReadableValue;
        }

        public T FromHumanReadable(string humanString)
        {
            return TranslationList.First(x => x.humanReadableValue == humanString).value;
        }

        public IEnumerable<string> GetAllHumanReadableValue()
        {
            return TranslationList.Select(x => x.humanReadableValue);
        }

    }
}