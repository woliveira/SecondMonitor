﻿namespace SecondMonitor.Contracts.NInject
{
    using Ninject;
    using Ninject.Planning.Bindings;
    using Ninject.Syntax;

    public abstract class AbstractBindingMetadataFactory<T>
    {
        private readonly IResolutionRoot _resolutionRoot;

        protected AbstractBindingMetadataFactory(IResolutionRoot resolutionRoot)
        {
            _resolutionRoot = resolutionRoot;
        }

        protected abstract string MetadataName { get; }

        public T Create(string bindingMetadataValue)
        {
            if (string.IsNullOrWhiteSpace(bindingMetadataValue))
            {
                return CreateEmpty();
            }

            try
            {
                return _resolutionRoot.Get<T>(bm => WhenMetadataMatched(bm, bindingMetadataValue));
            }
            catch (ActivationException)
            {
                return CreateEmpty();
            }
        }

        protected abstract T CreateEmpty();

        private bool WhenMetadataMatched(IBindingMetadata metadata, string bindingMetadataValue)
        {
            if (string.IsNullOrWhiteSpace(bindingMetadataValue))
            {
                return !metadata.Has(MetadataName);
            }

            if (!metadata.Has(MetadataName))
            {
                return false;
            }

            return metadata.Get<string>(MetadataName) == bindingMetadataValue;
        }
    }
}