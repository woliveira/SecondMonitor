﻿using System.Windows.Controls;

namespace SecondMonitor.WindowsControls.WPF.PluginsConfig
{
    /// <summary>
    /// Interaction logic for RemoteSettingsControl.xaml
    /// </summary>
    public partial class RemoteConfigurationControl : UserControl
    {
        public RemoteConfigurationControl()
        {
            InitializeComponent();
        }
    }
}
