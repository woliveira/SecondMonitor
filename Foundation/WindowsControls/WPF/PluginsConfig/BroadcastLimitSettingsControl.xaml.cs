﻿using System.Windows.Controls;

namespace SecondMonitor.WindowsControls.WPF.PluginsConfig
{
    /// <summary>
    /// Interaction logic for BroadcastLimitSettingsControl.xaml
    /// </summary>
    public partial class BroadcastLimitSettingsControl : UserControl
    {
        public BroadcastLimitSettingsControl()
        {
            InitializeComponent();
        }
    }
}
