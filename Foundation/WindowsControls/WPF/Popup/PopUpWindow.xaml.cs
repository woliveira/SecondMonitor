﻿using System.Windows;

namespace SecondMonitor.WindowsControls.WPF.Popup
{
    /// <summary>
    /// Interaction logic for PopUpWindow.xaml
    /// </summary>
    public partial class PopUpWindow : Window
    {
        public PopUpWindow()
        {
            InitializeComponent();
        }
    }
}
