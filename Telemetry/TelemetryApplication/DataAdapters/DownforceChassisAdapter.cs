﻿namespace SecondMonitor.Telemetry.TelemetryApplication.DataAdapters
{
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Systems;

    public class DownforceChassisAdapter : IChassisQuantityAdapter<Force>
    {
        private readonly TyreLoadAdapter _tyreLoadAdapter;

        public DownforceChassisAdapter(TyreLoadAdapter tyreLoadAdapter)
        {
            _tyreLoadAdapter = tyreLoadAdapter;
        }

        public bool IsQuantityComputed(SimulatorSourceInfo simulatorSource)
        {
            return !simulatorSource.TelemetryInfo.ContainsFrontRearDownforce;
        }

        public Force GetQuantityFront(SimulatorSourceInfo simulatorSource, CarInfo carInfo)
        {
            return IsQuantityComputed(simulatorSource) ?
                Force.GetFromNewtons(_tyreLoadAdapter.GetQuantityFromWheel(simulatorSource, carInfo.WheelsInfo.FrontLeft).InNewtons + _tyreLoadAdapter.GetQuantityFromWheel(simulatorSource, carInfo.WheelsInfo.FrontRight).InNewtons)
                : carInfo?.FrontDownForce ?? Force.GetFromNewtons(0) ;
        }

        public Force GetQuantityBack(SimulatorSourceInfo simulatorSource, CarInfo carInfo)
        {
            return IsQuantityComputed(simulatorSource) ?
                Force.GetFromNewtons(_tyreLoadAdapter.GetQuantityFromWheel(simulatorSource, carInfo.WheelsInfo.RearLeft).InNewtons + _tyreLoadAdapter.GetQuantityFromWheel(simulatorSource, carInfo.WheelsInfo.RearRight).InNewtons) :
                carInfo?.RearDownForce ?? Force.GetFromNewtons(0) ;
        }
    }
}