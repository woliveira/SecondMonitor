﻿namespace SecondMonitor.Telemetry.TelemetryApplication.DataAdapters
{
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Telemetry;

    public interface IQuantityAdapter<out T> where T: class, IQuantity
    {
        bool IsQuantityComputed(SimulatorSourceInfo sourceInfo);

        T GetQuantity(TelemetrySnapshot snapshot);
    }
}