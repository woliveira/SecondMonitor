﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Settings.DTO
{
    using Contracts;

    public class StintRenderingKindMap : AbstractHumanReadableMap<StintRenderingKind>
    {
        public StintRenderingKindMap()
        {
            TranslationList.Add((StintRenderingKind.None, "None (All Laps in single chart)"));
            TranslationList.Add((StintRenderingKind.SingleChart, "Separate series for each stint"));
            TranslationList.Add((StintRenderingKind.MultipleCharts, "Separate chart for each stint"));
        }
    }
}