﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels.GraphPanel.Chassis
{
    using System;
    using System.Linq;
    using DataAdapters;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Systems;
    using TelemetryManagement.DTO;

    public class FrontRearDownForceGraphViewModel : AbstractChassisGraphViewModel
    {
        private readonly DownforceChassisAdapter _downforceChassisAdapter;
        private string _title = "Front - Rear DownForce";

        public FrontRearDownForceGraphViewModel(DownforceChassisAdapter downforceChassisAdapter)
        {
            _downforceChassisAdapter = downforceChassisAdapter;
        }

        public override string Title => _title;
        protected override string YUnits => Force.GetUnitSymbol(UnitsCollection.ForceUnits);
        protected override double YTickInterval => Force.GetFromNewtons(1000).GetValueInUnits(UnitsCollection.ForceUnits);
        protected override bool CanYZoom => true;
        protected override Func<SimulatorSourceInfo, CarInfo, double> FrontFunc => (sourceInfo, x) => _downforceChassisAdapter.GetQuantityFront(sourceInfo, x).GetValueInUnits(UnitsCollection.ForceUnits);
        protected override Func<SimulatorSourceInfo, CarInfo, double> RearFunc => (sourceInfo, x) => _downforceChassisAdapter.GetQuantityBack(sourceInfo, x).GetValueInUnits(UnitsCollection.ForceUnits);

        protected override void PreviewOnLapLoaded(LapTelemetryDto lapTelemetryDto)
        {
            var firstDataPoint = lapTelemetryDto.DataPoints.FirstOrDefault();
            string newTitle;
            if (firstDataPoint == null || !_downforceChassisAdapter.IsQuantityComputed(firstDataPoint.SimulatorSourceInfo))
            {
                newTitle = "Front - Rear DownForce";
            }
            else
            {
                newTitle = "Front - Rear DownForce (C)";
            }

            if (newTitle != _title)
            {
                _title = newTitle;
                NotifyPropertyChanged(nameof(Title));
            }
            base.PreviewOnLapLoaded(lapTelemetryDto);
        }
    }
}