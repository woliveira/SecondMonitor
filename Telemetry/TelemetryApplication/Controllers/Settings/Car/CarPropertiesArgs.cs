﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Controllers.Settings.Car
{
    using System;
    using TelemetryApplication.Settings.DTO.CarProperties;

    public class CarPropertiesArgs : EventArgs
    {
        public CarPropertiesArgs(CarPropertiesDto carProperties)
        {
            CarProperties = carProperties;
        }

        public CarPropertiesDto CarProperties { get; }
    }
}