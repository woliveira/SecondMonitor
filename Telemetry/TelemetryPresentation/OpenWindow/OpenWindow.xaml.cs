﻿using System.Windows;

namespace SecondMonitor.TelemetryPresentation.OpenWindow
{
    /// <summary>
    /// Interaction logic for OpenWindow.xaml
    /// </summary>
    public partial class OpenWindow : Window
    {
        public OpenWindow()
        {
            InitializeComponent();
        }
    }
}
