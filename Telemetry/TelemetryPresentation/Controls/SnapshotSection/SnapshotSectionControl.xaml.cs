﻿using System.Windows.Controls;

namespace SecondMonitor.TelemetryPresentation.Controls.SnapshotSection
{
    /// <summary>
    /// Interaction logic for SnapshotSectionControl.xaml
    /// </summary>
    public partial class SnapshotSectionControl : UserControl
    {
        public SnapshotSectionControl()
        {
            InitializeComponent();
        }
    }
}
