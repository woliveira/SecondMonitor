﻿using System.Windows.Controls;

namespace SecondMonitor.TelemetryPresentation.Controls.Replay
{
    /// <summary>
    /// Interaction logic for ReplayControl.xaml
    /// </summary>
    public partial class ReplayControl : UserControl
    {
        public ReplayControl()
        {
            InitializeComponent();
        }
    }
}
