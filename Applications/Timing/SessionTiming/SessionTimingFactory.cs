﻿namespace SecondMonitor.Timing.SessionTiming
{
    using System.Collections.Generic;
    using Controllers;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using Drivers.ViewModel;
    using LapTimings;
    using NLog;
    using Rating.Application.Championship;
    using Rating.Application.Rating.RatingProvider;
    using Telemetry;
    using Timing.ViewModel;
    using TrackRecords.Controller;
    using ViewModel;
    using ViewModels.Factory;
    using ViewModels.SessionEvents;

    public class SessionTimingFactory
    {
        private readonly ISessionTelemetryControllerFactory _sessionTelemetryControllerFactory;
        private readonly ISessionRatingProvider _sessionRatingProvider;
        private readonly ITrackRecordsController _trackRecordsController;
        private readonly IChampionshipCurrentEventPointsProvider _championshipCurrentEventPointsProvider;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly DriverLapSectorsTrackerFactory _driverLapSectorsTrackerFactory;
        private readonly MapManagementController _mapManagementController;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly IBestLapEventProvider _bestLapEventProvider;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public SessionTimingFactory(ISessionTelemetryControllerFactory sessionTelemetryControllerFactory, ISessionRatingProvider sessionRatingProvider, ITrackRecordsController trackRecordsController, IChampionshipCurrentEventPointsProvider championshipCurrentEventPointsProvider,
        ISessionEventProvider sessionEventProvider, DriverLapSectorsTrackerFactory driverLapSectorsTrackerFactory, MapManagementController mapManagementController, IViewModelFactory viewModelFactory, IBestLapEventProvider bestLapEventProvider)
        {
            _viewModelFactory = viewModelFactory;
            _bestLapEventProvider = bestLapEventProvider;
            _sessionTelemetryControllerFactory = sessionTelemetryControllerFactory;
            _sessionRatingProvider = sessionRatingProvider;
            _trackRecordsController = trackRecordsController;
            _championshipCurrentEventPointsProvider = championshipCurrentEventPointsProvider;
            _sessionEventProvider = sessionEventProvider;
            _driverLapSectorsTrackerFactory = driverLapSectorsTrackerFactory;
            _mapManagementController = mapManagementController;
        }

        public SessionTimingController Create(SimulatorDataSet dataSet, TimingApplicationViewModel timingApplicationViewModel, bool invalidateFirstLap)
        {
            Logger.Info($"New Seesion Started :{dataSet.SessionInfo.SessionType.ToString()}");
            SessionTimingController timing = new SessionTimingController(timingApplicationViewModel, _sessionTelemetryControllerFactory.Create(dataSet), _sessionRatingProvider,
                _trackRecordsController, _championshipCurrentEventPointsProvider, _sessionEventProvider, _driverLapSectorsTrackerFactory, dataSet, _mapManagementController, _viewModelFactory, _bestLapEventProvider);
            Dictionary<string, DriverTiming> drivers = new Dictionary<string, DriverTiming>();
            foreach (DriverInfo driverInfo in dataSet.DriversInfo)
            {
                if (drivers.ContainsKey(driverInfo.DriverSessionId))
                {
                    continue;
                }
                //drivers[driverInfo.DriverSessionId] = DriverTiming.FromModel(driverInfo, timing, _driverLapSectorsTrackerFactory, false);
                drivers[driverInfo.DriverSessionId] = DriverTiming.FromModel(driverInfo, timing, _driverLapSectorsTrackerFactory, _bestLapEventProvider, invalidateFirstLap);
            }

            timing.SetDrivers(dataSet, drivers.Values);
            return timing;
        }
    }
}