﻿namespace SecondMonitor.Timing.SessionTiming.Drivers.ViewModel
{
    using System;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using SecondMonitor.DataModel.Snapshot.Drivers;
    using DataModel.Telemetry;

    public class LapTelemetryInfo
    {

        private static readonly InputInfo BlankInput = new InputInfo();
        private bool _waitForSessionStart;

        public LapTelemetryInfo(DriverInfo driverInfo, SimulatorDataSet dataSet, LapInfo lapInfo, TimeSpan snapshotInterval, SimulatorSourceInfo simulatorSourceInfo)
        {
            LapStarSnapshot = new TelemetrySnapshot(driverInfo, dataSet.SessionInfo.WeatherInfo, dataSet.InputInfo, simulatorSourceInfo);
            LapInfo = lapInfo;
            PortionTimes = new LapPortionTimes(dataSet.SessionInfo.TrackInfo.LayoutLength.InMeters, lapInfo);
            TimedTelemetrySnapshots = new TimedTelemetrySnapshots(snapshotInterval);

            _waitForSessionStart = dataSet.SessionInfo.SessionType == SessionType.Race && (dataSet.SessionInfo.SessionPhase == SessionPhase.Countdown || dataSet.SessionInfo.SessionPhase == SessionPhase.Unavailable);
        }

        public TelemetrySnapshot LapEndSnapshot { get; private set; }
        public TelemetrySnapshot LapStarSnapshot { get; private set; }
        public TimedTelemetrySnapshots TimedTelemetrySnapshots { get; private set; }
        public bool IsPurged { get; private set; }
        public LapPortionTimes PortionTimes { get; private set; }
        public LapInfo LapInfo { get; }

        public void CreateLapEndSnapshot(DriverInfo driverInfo, WeatherInfo weather, InputInfo inputInfo, SimulatorSourceInfo simulatorSourceInfo)
        {
            LapEndSnapshot = new TelemetrySnapshot(driverInfo, weather, inputInfo, simulatorSourceInfo);
        }

        public void UpdateTelemetry(SimulatorDataSet dataSet, DriverInfo driverInfo)
        {
            if (IsPurged)
            {
                throw new InvalidOperationException("Cannot update Telemetry on a purged TelemetryInfo");
            }

            if (_waitForSessionStart && dataSet.SessionInfo.SessionPhase == SessionPhase.Green)
            {
                LapStarSnapshot = new TelemetrySnapshot(driverInfo, dataSet.SessionInfo.WeatherInfo, dataSet.InputInfo, dataSet.SimulatorSourceInfo);
                _waitForSessionStart = false;
            }

            PortionTimes.UpdateLapPortions();
            if (!driverInfo.InPits)
            {
                var inputInfoToUse = driverInfo.IsPlayer ? dataSet.InputInfo : BlankInput;
                TimedTelemetrySnapshots.AddNextSnapshot(LapInfo.CurrentlyValidProgressTime, driverInfo, dataSet.SessionInfo.WeatherInfo, inputInfoToUse, dataSet.SimulatorSourceInfo);
            }
        }

        public void Complete(Distance lapDistance)
        {
            TimedTelemetrySnapshots.TrimInvalid(lapDistance);
        }

        public void Purge()
        {
            if (IsPurged)
            {
                return;
            }

            PortionTimes = null;
            TimedTelemetrySnapshots = null;
            IsPurged = true;
        }
    }
}