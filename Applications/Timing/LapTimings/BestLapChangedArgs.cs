﻿namespace SecondMonitor.Timing.LapTimings
{
    using System;
    using SessionTiming.Drivers.ViewModel;

    public class BestLapChangedArgs : EventArgs
    {
        public BestLapChangedArgs(ILapInfo oldLap, ILapInfo newLap)
        {
            OldLap = oldLap;
            NewLap = newLap;
        }

        public ILapInfo OldLap { get; }
        public ILapInfo NewLap { get; }
    }
}