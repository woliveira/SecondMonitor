﻿namespace SecondMonitor.Timing.LapTimings
{
    using System;
    using SessionTiming.Drivers.ViewModel;

    public class BestLapEventProvider : IBestLapEventProvider
    {
        public event EventHandler<BestLapChangedArgs> BestLapPersonalChanged;
        public event EventHandler<BestLapChangedArgs> BestLapChanged;
        public event EventHandler<BestLapChangedArgs> BestClassLapChanged;
        public event EventHandler<BestSectorChangedArgs> BestSectorChanged;
        public event EventHandler<BestSectorChangedArgs> BestSectorClassChanged;

        public void NotifyBestLapChanged(object sender, ILapInfo oldLap, ILapInfo newLap)
        {
            BestLapChanged?.Invoke(sender, new BestLapChangedArgs(oldLap, newLap));
        }

        public void NotifyBestClassLapChanged(object sender, ILapInfo oldLap, ILapInfo newLap)
        {
            BestClassLapChanged?.Invoke(sender, new BestLapChangedArgs(oldLap, newLap));
        }

        public void NotifyBestLapPersonalChanged(object sender, ILapInfo oldLap, ILapInfo newLap)
        {
            BestLapPersonalChanged?.Invoke(sender, new BestLapChangedArgs(oldLap, newLap));
        }

        public void NotifyBestSectorChanged(object sender, SectorTiming oldSector, SectorTiming newSector)
        {
            BestSectorChanged?.Invoke(sender, new BestSectorChangedArgs(oldSector, newSector));
        }

        public void NotifyBestSectorClassChanged(object sender, SectorTiming oldSector, SectorTiming newSector)
        {
            BestSectorClassChanged?.Invoke(sender, new BestSectorChangedArgs(oldSector, newSector));
        }
    }
}