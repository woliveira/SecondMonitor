﻿namespace SecondMonitor.Timing.Telemetry
{
    using System.Threading.Tasks;
    using SessionTiming.Drivers.ViewModel;
    using ViewModels.Controllers;

    public interface ISessionTelemetryController : IController
    {
        Task<bool> TrySaveLapTelemetry(ILapInfo lapInfo);
    }
}