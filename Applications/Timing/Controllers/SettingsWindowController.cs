﻿namespace SecondMonitor.Timing.Controllers
{
    using System.Threading.Tasks;
    using System.Windows;
    using Contracts.Commands;
    using Presentation.View;
    using ViewModels.Controllers;
    using ViewModels.Factory;
    using ViewModels.Layouts;
    using ViewModels.Settings;

    public class SettingsWindowController : IController
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly ILayoutEditorManipulator _layoutEditorManipulator;
        private DisplaySettingsWindow _settingsWindow;

        public SettingsWindowController(ISettingsProvider settingsProvider, IViewModelFactory viewModelFactory, ILayoutEditorManipulator layoutEditorManipulator)
        {
            _settingsProvider = settingsProvider;
            _viewModelFactory = viewModelFactory;
            _layoutEditorManipulator = layoutEditorManipulator;
            _layoutEditorManipulator.ApplyLayoutCommand = new RelayCommand(ApplyNewLayout);
        }

        public void OpenSettingWindow()
        {
            if (_settingsWindow != null && _settingsWindow.IsVisible)
            {
                _settingsWindow.Focus();
                return;
            }

            CreateSettingsWindow();
        }

        private void CreateSettingsWindow()
        {
            _layoutEditorManipulator.SetLayoutDescription(_settingsProvider.DisplaySettingsViewModel.LayoutSettingsViewModel.LayoutDescription);
            _settingsWindow = new DisplaySettingsWindow
            {
                DataContext = _settingsProvider.DisplaySettingsViewModel,
                Owner = Application.Current.MainWindow,
            };
            _settingsWindow.Show();
        }

        public Task StartControllerAsync()
        {
            _settingsProvider.DisplaySettingsViewModel.DefaultLayoutCallback = _layoutEditorManipulator.CreateDefaultLayout;
            _settingsProvider.DisplaySettingsViewModel.LayoutEditorViewModel = _layoutEditorManipulator.LayoutEditorViewModel;
            if (_settingsProvider.DisplaySettingsViewModel.LayoutSettingsViewModel.LayoutDescription == null)
            {
                _settingsProvider.DisplaySettingsViewModel.LayoutSettingsViewModel.LayoutDescription = _layoutEditorManipulator.CreateDefaultLayout();
            }

            return Task.CompletedTask;
        }

        private void ApplyNewLayout()
        {
            _settingsProvider.DisplaySettingsViewModel.LayoutSettingsViewModel.LayoutDescription = _layoutEditorManipulator.GetLayoutDescription();
        }

        public Task StopControllerAsync()
        {
            return Task.CompletedTask;
        }
    }
}