﻿using System.Windows.Controls;

namespace ControlTestingApp.Controls
{
    /// <summary>
    /// Interaction logic for FuelCalculatorTestControl.xaml
    /// </summary>
    public partial class FuelCalculatorTestControl : UserControl
    {
        public FuelCalculatorTestControl()
        {
            InitializeComponent();
        }
    }
}
