﻿using System.Windows.Controls;

namespace ControlTestingApp.Controls
{
    /// <summary>
    /// Interaction logic for GeometryTestControl.xaml
    /// </summary>
    public partial class GeometryTestControl : UserControl
    {
        public GeometryTestControl()
        {
            InitializeComponent();
        }
    }
}
