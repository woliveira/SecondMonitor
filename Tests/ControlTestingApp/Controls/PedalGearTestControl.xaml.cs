﻿using System.Windows.Controls;

namespace ControlTestingApp.Controls
{
    /// <summary>
    /// Interaction logic for PedalGearTestControl.xaml
    /// </summary>
    public partial class PedalGearTestControl : UserControl
    {
        public PedalGearTestControl()
        {
            InitializeComponent();
        }
    }
}
