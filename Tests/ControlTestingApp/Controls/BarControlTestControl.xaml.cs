﻿using System.Windows.Controls;

namespace ControlTestingApp.Controls
{
    /// <summary>
    /// Interaction logic for BarControlTestControl.xaml
    /// </summary>
    public partial class BarControlTestControl : UserControl
    {
        public BarControlTestControl()
        {
            InitializeComponent();
        }
    }
}
